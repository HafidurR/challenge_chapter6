### My Repository to clone

https://gitlab.com/HafidurR/challenge_chapter6
### Installation

1. Clone the repo
2. Install NPM packages
   ```sh
   npm install
   ```

## Create Database and Table

- run node `sequelize db:create`
- run node `sequelize db:migrate`

## Create Database and Table Test

- run node `NODE_ENV=test sequelize db:create`
- run node `NODE_ENV=test sequelize db:migrate`

##  Run the Program

- node `app.js` 
##  Run the Test

- `npx jest user.test.js` 
- `npx jest author.test.js` 
- `npx jest category.test.js` 
- `npx jest book.test.js` 

##  Test the Program with Heroku

`- https://floating-tundra-29230.herokuapp.com

## Alur forgot password
- Gunakan endpoint forgot-password
- Lalu verifikasi kode otp yang dikiri oleh endpoint pertama melalui endpoint verify-otp/:otp 
- Terakhir ubah password melalui endpoint :email/change-password/:otp