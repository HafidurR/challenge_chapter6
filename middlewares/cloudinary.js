const cloudinary  = require('cloudinary').v2;
// const fs          = require('fs');

cloudinary.config({
  cloud_name: 'mis-bustanul-abidin',
  api_key: '599621175414463',
  api_secret: 'mywkcEbyBNntSLJ0iHlxn20B5Jc'
});


const upload_cloudinary = async (req, res, next) => {
  try {
    const foldering = `my-asset/${req.file.mimetype.split('/')[0]}`;
    const uploadResult = await cloudinary.uploader.upload(req.file.path, {
      folder: foldering
    });
    req.body.image = uploadResult.secure_url;
    next();
  } catch (error) {
    console.log(error);
  }
}

const upload_video_cloudinary = async (req, res, next) => {
  try {
    const foldering = `my-video/${req.file.mimetype.split("/")[0]}`;
    const uploadResult = await cloudinary.uploader.upload(req.file.path, {
      resource_type: "video",
      folder: foldering,
    });
    // fs.unlinkSync(req.file.path);
    req.body.video = uploadResult.secure_url;
    next();
  } catch (error) {
    // fs.unlinkSync(req.file.path);
    console.log(error);
  }

}

module.exports = {
  upload_cloudinary,
  upload_video_cloudinary
};