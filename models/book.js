module.exports = (sequelize, DataTypes) => {
  const Book = sequelize.define('Book', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Title is required'
        },
        notEmpty: {
          args: true,
          msg: "Title cannot be empty",
        }
      }
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Description is required'
        },
        notEmpty: {
          args: true,
          msg: "Description cannot be empty",
        }
      }
    },
    publisher: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Publisher is required'
        },
        notEmpty: {
          args: true,
          msg: "Publisher cannot be empty",
        }
      }
    },
    publish_at: {
      type: DataTypes.DATE,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Publish_at is requirded'
        },
        notEmpty: {
          args: true,
          msg: "Publish_at cannot be empty",
        }
      }
    },
    author_id: {
      allowNull: false,
      type: DataTypes.INTEGER,
      validate: {
        notNull: {
          msg: 'Author_id is required'
        },
        notEmpty: {
          args: true,
          msg: "Author_id cannot be empty",
        },
        isInt: {
          args: true,
          msg: "Author_id must be an integer value",
        }
      }
    },
    category_id: {
      allowNull: false,
      type: DataTypes.INTEGER,
      validate: {
        notNull: {
          msg: 'Category_id is required'
        },
        notEmpty: {
          args: true,
          msg: "Category_id cannot be empty",
        },
        isInt: {
          args: true,
          msg: "Category_id must be an integer value",
        },
      }
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    tableName: 'books'
  })

  Book.associate = models => {
    Book.belongsTo(models.Author, {
      foreignKey: 'author_id'
    });
    Book.belongsTo(models.Category, {
      foreignKey: 'category_id'
    });
    
  }
  return Book;
}