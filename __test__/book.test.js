const app = require("../app.js");
const request = require("supertest");
const { User, Author, Book, Category } = require("../models");

let token, id_author, id_category;

const user_register = {
  name: "user_testing@gmail.com",
  email: "user_testing@mail.com",
  password: "user123",
  birthdate: "2000-04-10T00:00:00.000Z",
  no_telp: "081234567890",
  address: "Probolinggo Jawa Timur",
  role: "admin",
  image: "www.image.com"
};

const user_login = {
  email: "user_testing@mail.com",
  password: "user123",
};

const book_create = {
  title: "Harry Potter",
  description: "Boku ini menceritakan tentang sorang penyihir muda diakademi sihir",
  publisher: "Gramedia",
  publish_at: "2010-01-01T00:00:02.012Z",
  author_id: 1,
  category_id: 1
};

const book_update = {
  title: "Harry Potter 2",
  description: "Boku ini menceritakan tentang sorang penyihir muda diakademi sihir",
  publisher: "Gramedia Online",
  publish_at: "2010-01-01T00:00:02.012Z",
  author_id: 1,
  category_id: 1
};


beforeAll((done) => {
  request(app)
    .post("/users/register")
    .send(user_register)
    .end((err, res) => {
      if (err) {
        console.log(err);
        done();
      } else {
        done();
      }
    });
});

beforeEach((done) => {
  request(app)
    .post("/users/login")
    .send(user_login)
    .end(async (err, res) => {
      if (err) {
        console.log(err);
        done();
      } else {
        token = res.body.token;
        done();
      }
    });
});

describe("Create Book", () => {
  test("successfully create book", (done) => {
    request(app)
      .post(`/books`)
      .set("x-access-token", token)
      .send(book_create)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.body).toHaveProperty("book");
          expect(res.body.book.id).toBeGreaterThan(0);
          expect(res.body.book.title).toBe(book_create.title);
          expect(res.body.book.description).toBe(book_create.description);
          expect(res.body.book.publisher).toBe(book_create.publisher);
          expect(res.body.book.publish_at).toBe(book_create.publish_at);
          expect(res.body.book.author_id).toBe(book_create.author_id);
          expect(res.body.book.category_id).toBe(book_create.category_id);
          expect(res.statusCode).toBe(201);
          done();
        }
      });
  });

  test('failed post author', (done) => {
    const book_create_failed = {
      title: "Harry Potter",
      description: "Boku ini menceritakan tentang sorang penyihir muda diakademi sihir",
      publish_at: 2012,
      author_id: 1,
      category_id: 1
    };
    request(app)
      .post('/books')
      .set("x-access-token", token)
      .send(book_create_failed)
      .end(function (err, res) {
        if (err) {
          done(err)
        }
        expect(res.status).toEqual(400);
        expect(res.body.status).toEqual("error");
        expect(res.body.message).toEqual("notNull Violation: Publisher is required");

        done()
      })
  })
});

describe("Get & Put Book", () => {
  let book_instance;
  beforeAll((done) => {
    request(app)
      .post("/books")
      .send(book_create)
      .end(async (err) => {
        if (err) {
          console.log(err);
          done();
        } else {
          book_instance = await Book.findAll({ where: { id: 1 } });
          done();
        }
      });
  });

  test('successfully get book', (done) => {
    request(app)
      .get(`/books?page=1&row=3`)
      .end(function (err, res) {
        if (err) {
          done(err)
        }
        expect(typeof res).toBe("object");
        expect(res.body).toHaveProperty("book");
        expect(Object.keys(book_instance).length).toBe(1);
        expect(res.body.book[0].title).toBe(book_create.title);
        expect(res.body.book[0].description).toBe(book_create.description);
        expect(res.body.book[0].publisher).toBe(book_create.publisher);
        expect(res.body.book[0].publish_at).toBe(book_create.publish_at);
        expect(res.statusCode).toBe(200);

        done()
      })
  })

  test('failed get author', (done) => {
    request(app)
      .get(`/categories/1`)
      .end(function (err, res) {
        if (err) {
          done(err)
        }
        expect(res.status).toEqual(403)
        expect(typeof res).toBe("object")
        expect(res.body).toHaveProperty("status")
        expect(res.body).toHaveProperty("message")
        expect(res.body.status).toEqual("forbidden")
        expect(res.body.message).toEqual("Access token required")

        done()
      })
  })

  test("successfully update book", (done) => {
    request(app)
      .put(`/books/${book_instance[0].dataValues.id}`)
      .set("x-access-token", token)
      .send(book_update)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.body).toHaveProperty("Book");
          expect(Object.keys(book_instance).length).toBe(1);
          expect(res.body.Book.title).toBe(book_update.title);
          expect(res.body.Book.description).toBe(book_update.description);
          expect(res.body.Book.publisher).toBe(book_update.publisher);
          expect(res.body.Book.publish_at).toBe(book_update.publish_at);
          expect(res.statusCode).toBe(200);
          done();
        }
      });
  });

  test('failed update book', (done) => {
    request(app)
      .put(`/books/100`)
      .set("x-access-token", token)
      .end(function (err, res) {
        if (err) {
          done(err)
        }

        expect(res.status).toEqual(404);
        expect(typeof res).toBe("object");
        expect(res.body).toHaveProperty("message");
        expect(res.body.message).toEqual("Data not found");

        done()
      })
  })
});

afterAll((done) => {
  User.destroy({ where: { email: "user_testing@gmail.com" } })
    .then(() => {
      console.log("successfully delete user testing");
      done();
    })
    .catch((err) => {
      console.log(err);
      done();
    });
});