const app = require("../app.js");
const request = require("supertest");
const { User } = require("../models");

const user_register = {
  name: "user_testing@gmail.com",
  email: "user_testing@mail.com",
  password: "user123",
  birthdate: "2000-04-10T00:00:00.000Z",
  no_telp: "081234567890",
  address: "Probolinggo Jawa Timur",
  role: "admin",
  image: "www.image.com"
};

const user_login = {
  email: "user_testing@mail.com",
  password: "user123",
};

describe("Testing Register", () => {
  test("successfully register", (done) => {
    request(app)
      .post("/users/register")
      .send(user_register)
      .end((err, res) => {
        if (err) {
          console.log(err);
          done();
        } else {
          expect(res.body).toHaveProperty("data");
          expect(res.body.data).not.toBeNull();
          expect(res.body.data.name).toBe(user_register.name);
          expect(res.body.data.email).toBe(user_register.email);
          expect(res.body.data.birthdate).toBe(user_register.birthdate);
          expect(res.body.data.no_telp).toBe(user_register.no_telp);
          expect(res.body.data.address).toBe(user_register.address);
          expect(res.body.data.image).toBe(user_register.image);
          expect(res.statusCode).toBe(201);
          done();
        }
      });
  });

  afterEach((done) => {
    User.destroy({ where: { email: "user_testing@gmail.com" } })
      .then(() => {
        console.log("successfully delete user testing");
        done();
      })
      .catch((err) => {
        console.log(err);
        done();
      });
  });
});

describe("Testing login", () => {
  beforeEach((done) => {
    request(app)
      .post("/users/register")
      .send(user_register)
      .end((err, res) => {
        if (err) {
          console.log(err);
          done();
        } else {
          done();
        }
      });
  });

  test("successfully login", (done) => {
    request(app)
      .post("/users/login")
      .send(user_login)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.body).toHaveProperty("token");
          expect(res.body.token).not.toBeUndefined();
          expect(res.body.token).not.toBeNull();
          expect(res.body.token).not.toHaveLength(0);
          expect(res.statusCode).toBe(200);
          done();
        }
      });
  });

  afterEach((done) => {
    User.destroy({ where: { email: "user_testing@gmail.com" } })
      .then(() => {
        console.log("successfully delete user testing");
        done();
      })
      .catch((err) => {
        console.log(err);
        done();
      });
  });
});

afterAll((done) => {
  User.destroy({ where: { email: "user_testing@gmail.com" } })
    .then(() => {
      console.log("successfully delete user testing");
      done();
    })
    .catch((err) => {
      console.log(err);
      done();
    });
});

afterAll((done) => {
  User.destroy({ where: { email: "user_testing@gmail.com" } })
    .then(() => {
      console.log("successfully delete user testing");
      done();
    })
    .catch((err) => {
      console.log(err);
      done();
    });
});