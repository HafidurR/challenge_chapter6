const app = require("../app.js");
const request = require("supertest");
const { User, Author } = require("../models");

let token, id_author;

const user_register = {
  name: "user_testing@gmail.com",
  email: "user_testing@mail.com",
  password: "user123",
  birthdate: "2000-04-10T00:00:00.000Z",
  no_telp: "081234567890",
  address: "Probolinggo Jawa Timur",
  role: "admin",
  image: "www.image.com"
};

const user_login = {
  email: "user_testing@mail.com",
  password: "user123",
};

const author_create = {
  name: "M. Hafidurrohman",
  birthdate: "2000-04-10T00:00:00.000Z",
  gender: "pria",
  address: "Probolinggo Jawa Timur"
};

const author_update = {
  name: "M. Hafidurrohman",
  birthdate: "2000-04-10T00:00:00.000Z",
  gender: "pria",
  address: "Paiton Probolinggo Jawa Timur"
};

beforeAll((done) => {
  request(app)
    .post("/users/register")
    .send(user_register)
    .end((err, res) => {
      if (err) {
        console.log(err);
        done();
      } else {
        done();
      }
    });
});

beforeEach((done) => {
  request(app)
    .post("/users/login")
    .send(user_login)
    .end(async (err, res) => {
      if (err) {
        console.log(err);
        done();
      } else {
        token = res.body.token;
        // console.log(res.body.token);
        done();
      }
    });
});

describe("Create Author", () => {
  test("successfully create author", (done) => {
    request(app)
      .post(`/authors`)
      .set("x-access-token", token)
      .send(author_create)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.body).toHaveProperty("author");
          expect(res.body.author.name).toBe(author_create.name);
          expect(res.body.author.birthdate).toBe(author_create.birthdate);
          expect(res.body.author.gender).toBe(author_create.gender);
          expect(res.body.author.address).toBe(author_create.address);
          expect(res.statusCode).toBe(201);
          done();
        }
      });
  });

  test('failed post author', (done) => {
    const author_create_failed = {
      name: "M. Hafidurrohman",
      birthdate: "2000-04-10T00:00:00.000Z",
      gender: "pria",
    };
    request(app)
      .post('/authors')
      .set("x-access-token", token)
      .send(author_create_failed)
      .end(function (err, res) {
        if (err) {
          done(err)
        }
        expect(res.status).toEqual(400);
        expect(res.body.status).toEqual("error");
        expect(res.body.message).toEqual([{ "address": "Address is required" }]);

        done()
      })
  })
});

describe("Get & Put Author", () => {
  let author_instance;
  beforeAll((done) => {
    request(app)
      .post("/authors")
      .send(author_create)
      .end(async (err) => {
        if (err) {
          console.log(err);
          done();
        } else {
          author_instance = await Author.findAll({ where: { id: 1 } });
          done();
        }
      });
  });

  test('successfully get author', (done) => {
    request(app)
      .get(`/authors?page=1&row=3`)
      .end(function (err, res) {
        if (err) {
          done(err)
        }
        expect(typeof res).toBe("object");
        expect(res.body).toHaveProperty("author");
        expect(Object.keys(author_instance).length).toBe(1);
        expect(res.body.author[0].name).toBe(author_create.name);
        expect(res.body.author[0].address).toBe(author_create.address);
        expect(res.statusCode).toBe(200);

        done()
      })
  })

  test('failed get author', (done) => {
    request(app)
      .get(`/authors/1`)
      .end(function (err, res) {
        if (err) {
          done(err)
        }
        expect(res.status).toEqual(403)
        expect(typeof res).toBe("object")
        expect(res.body).toHaveProperty("status")
        expect(res.body).toHaveProperty("message")
        expect(res.body.status).toEqual("forbidden")
        expect(res.body.message).toEqual("Access token required")

        done()
      })
  })

  test("successfully update author", (done) => {
    request(app)
      .put(`/authors/${author_instance[0].dataValues.id}`)
      .set("x-access-token", token)
      .send(author_update)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          // let test = res.body;
          expect(res.body).toHaveProperty("Author");
          expect(Object.keys(author_instance).length).toBe(1);
          expect(res.body.Author.name).toBe(author_update.name);
          expect(res.body.Author.birthdate).toBe(author_update.birthdate);
          expect(res.body.Author.gender).toBe(author_update.gender);
          expect(res.body.Author.address).toBe(author_update.address);
          expect(res.statusCode).toBe(200);
          done();
        }
      });
  });

  test('failed update author', (done) => {
    request(app)
      .put(`/authors/100`)
      .set("x-access-token", token)
      .end(function (err, res) {
        if (err) {
          done(err)
        }

        expect(res.status).toEqual(404);
        expect(typeof res).toBe("object");
        expect(res.body).toHaveProperty("message");
        expect(res.body.message).toEqual("Data not found");

        done()
      })
  })
});

afterAll((done) => {
  User.destroy({ where: { email: "user_testing@gmail.com" } })
    .then(() => {
      console.log("successfully delete user testing");
      done();
    })
    .catch((err) => {
      console.log(err);
      done();
    });
});