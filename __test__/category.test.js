const app = require("../app.js");
const request = require("supertest");
const { User, Category } = require("../models");

let token;

const user_register = {
  name: "user_testing@gmail.com",
  email: "user_testing@mail.com",
  password: "user123",
  birthdate: "2000-04-10T00:00:00.000Z",
  no_telp: "081234567890",
  address: "Probolinggo Jawa Timur",
  role: "admin",
  image: "www.image.com"
};

const user_login = {
  email: "user_testing@mail.com",
  password: "user123",
};

const category_create = {
  category_name: "Action"
};

const category_update = {
  category_name: "Action and Mystery",
};

beforeAll((done) => {
  request(app)
    .post("/users/register")
    .send(user_register)
    .end((err, res) => {
      if (err) {
        console.log(err);
        done();
      } else {
        done();
      }
    });
});

beforeEach((done) => {
  request(app)
    .post("/users/login")
    .send(user_login)
    .end(async (err, res) => {
      if (err) {
        console.log(err);
        done();
      } else {
        token = res.body.token;
        done();
      }
    });
});

describe("Create Category", () => {
  test("successfully create category", (done) => {
    request(app)
      .post(`/categories`)
      .set("x-access-token", token)
      .send(category_create)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.body).toHaveProperty("category");
          expect(res.body.category.id).toBeGreaterThan(0);
          expect(res.body.category.category_name).toBe(category_create.category_name);
          expect(res.statusCode).toBe(201);
          done();
        }
      });
  });

  test('failed post author', (done) => {
    const author_create_failed = {
      category_name: ""
    };
    request(app)
      .post('/categories')
      .set("x-access-token", token)
      .send(author_create_failed)
      .end(function (err, res) {
        if (err) {
          done(err)
        }
        expect(res.status).toEqual(400);
        expect(res.body.status).toEqual("error");
        expect(res.body.message).toEqual([{ "category_name":"Category_name cannot be empty" }]);

        done()
      })
  })
});

describe("Get & Put Category", () => {
  let category_instance;
  beforeAll((done) => {
    request(app)
      .post("/categories")
      .send(category_create)
      .end(async (err) => {
        if (err) {
          console.log(err);
          done();
        } else {
          category_instance = await Category.findAll({ where: { id: 1 } });
          done();
        }
      });
  });

  test('successfully get author', (done) => {
    request(app)
      .get(`/categories?page=1&row=3`)
      .end(function (err, res) {
        if (err) {
          done(err)
        }
        expect(typeof res).toBe("object");
        expect(res.body).toHaveProperty("category");
        expect(Object.keys(category_instance).length).toBe(1);
        expect(res.body.category[0].category_name).toBe(category_create.category_name);
        expect(res.statusCode).toBe(200);

        done()
      })
  })

  test('failed get author', (done) => {
    request(app)
      .get(`/categories/1`)
      .end(function (err, res) {
        if (err) {
          done(err)
        }
        expect(res.status).toEqual(403)
        expect(typeof res).toBe("object")
        expect(res.body).toHaveProperty("status")
        expect(res.body).toHaveProperty("message")
        expect(res.body.status).toEqual("forbidden")
        expect(res.body.message).toEqual("Access token required")

        done()
      })
  })

  test("successfully update author", (done) => {
    request(app)
      .put(`/categories/${category_instance[0].dataValues.id}`)
      .set("x-access-token", token)
      .send(category_update)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.body).toHaveProperty("Category");
          console.log(res.body.Category.category_name);
          expect(Object.keys(category_instance).length).toBe(1);
          expect(res.body.Category.category_name).toBe(category_update.category_name);
          expect(res.statusCode).toBe(200);
          done();
        }
      });
  });

  test('failed update author', (done) => {
    request(app)
      .put(`/categories/100`)
      .set("x-access-token", token)
      .end(function (err, res) {
        if (err) {
          done(err)
        }

        expect(res.status).toEqual(404);
        expect(typeof res).toBe("object");
        expect(res.body).toHaveProperty("message");
        expect(res.body.message).toEqual("Data not found");

        done()
      })
  })
});

afterAll((done) => {
  User.destroy({ where: { email: "user_testing@gmail.com" } })
    .then(() => {
      console.log("successfully delete user testing");
      done();
    })
    .catch((err) => {
      console.log(err);
      done();
    });
});