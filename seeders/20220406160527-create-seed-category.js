'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('categories', [{
      category_name: 'Horror',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      category_name: 'Action',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      category_name: 'Anime',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('authors', null, {});
     */
    await queryInterface.bulkDelete('categories', null, {});
  }
};
