'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('authors', [{
      name: 'M. Hafidurrohman',
      birthdate: '2001-04-04',
      gender: 'pria',
      address: 'Paiton Probolinggo Jawa Timur Indonesia',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Nadiatul Khoir',
      birthdate: '2000-12-04',
      gender: 'wanita',
      address: 'Paiton Probolinggo Jawa Timur Indonesia',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'M. Shobri',
      birthdate: '2000-06-16',
      gender: 'pria',
      address: 'Kraksaan Probolinggo Jawa Timur Indonesia',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('authors', null, {});
     */
    await queryInterface.bulkDelete('authors', null, {});
  }
};
