require('dotenv').config();
const express       = require('express');
const path          = require('path');
const cookieParser  = require('cookie-parser');
const logger        = require('morgan');
const app           = express();
const port          = process.env.PORT || 3000;
const authors       = require('./routes/author.routes');
const books         = require('./routes/book.routes');
const categories    = require("./routes/category.routes");
const users         = require("./routes/user.routes");
const video         = require("./routes/video.routes");
const swaggerJSON   = require('./api-docs/swagger-output.json');
const swaggerUI     = require('swagger-ui-express');

app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/authors', authors);
app.use('/books', books);
app.use("/categories", categories);
app.use("/users", users);
app.use("/video-upload", video);

module.exports = app;

