const swaggerAutogen = require('swagger-autogen')();

const doc = {
  info: {
    title: 'My Libarary',
    description: 'This is an simple CRUD of book and also author.',
  },
  host: 'localhost:3000',
  schemes: ['http'],
};

const outputFile = './api-docs/swagger-output.json';
const endpointsFiles = ['./app.js'];

/* NOTE: if you use the express Router, you must pass in the 
   'endpointsFiles' only the root file where the route starts,
   such as index.js, app.js, routes.js, ... */

swaggerAutogen(outputFile, endpointsFiles, doc);