const { Book, Author, Category } = require('../models');

class bookController {
  static getAll = async (req, res) => {
    let { page, row } = req.query;

    page -= 1;
    await Book.findAll({
      attributes: ['id', 'title', 'description', 'publisher', 'publish_at'],
      include: [
        {
          model: Author,
          attributes: ['id', 'name', 'address']
        },
        {
          model: Category,
          attributes: ['id', 'category_name']
        }
      ],
      offset: page,
      limit: row
    })
      .then((result) => {
        return res.status(200).json({
          message: 'success get all data',
          book: result
        })
      })
      .catch((err) => {
        return res.status(400).json({
          status: 'error',
          message: err.message
        })
      })
  }

  static getByID = async (req, res) => {
    const id = req.params.id;
    const roleUser = req.user.role;

    if (roleUser === 'user') {
      return res.status(401).json({
        status: "error",
        message: "user unauthorized"
      })
    } else {
      await Book.findOne({
        where: {
          id: id
        }, attributes: ['id', 'title', 'description', 'publisher', 'publish_at', 'author_id'],
        include: [
          {
            model: Author,
            attributes: ['id', 'name', 'address']
          },
          {
            model: Category,
            attributes: ['id', 'category_name']
          }
        ]
      })
        .then((result) => {
          if (result === null) {
            return res.status(404).json({
              status: 'error',
              message: 'Data not found'
            })
          } else {
            return res.status(200).json({
              message: 'success get by ID',
              book: result
            })
          }
        })
        .catch((err) => {
          return res.status(500).json({
            status: 'error',
            message: err.message
          })
        })
    }
  }

  static create = async (req, res) => {
    const { title, description, publisher, publish_at, author_id, category_id } = req.body;
    const roleUser = req.user.role;

    if (roleUser === 'user') {
      return res.status(401).json({
        status: "error",
        message: "user unauthorized"
      })
    } else {
      await Book.create({
        title, description, publisher, publish_at, author_id, category_id
      })
        .then((result) => {
          return res.status(201).json({
            message: 'success create new author',
            book: result
          })
        })
        .catch((error) => {
          return res.status(400).json({
            status: 'error',
            message: error.message
          })
        })
    }
  }

  static update = async (req, res) => {
    const id = req.params.id;
    const { title, description, publisher, publish_at, author_id, category_id } = req.body;
    const roleUser = req.user.role;

    if (roleUser === 'user') {
      return res.status(401).json({
        status: "error",
        message: "user unauthorized"
      })
    } else {
      await Book.findOne({
        where: {
          id: id
        }
      })
        .then(async (result) => {
          if (result === null) {
            return res.status(404).json({
              status: 'error',
              message: 'Data not found'
            })
          } else {
            await Book.update({
              title, description, publisher, publish_at, author_id, category_id
            }, {
              where: {
                id: id
              }
            })
              .then(async () => {
                await Book.findOne({
                  where: {
                    id: id
                  }
                })
                  .then(rsl => {
                    return res.status(200).json({
                      message: 'success update author',
                      Book: rsl
                    })
                  }).catch(err => {
                    return res.status(400).json({
                      message: err.message
                    })
                  })
              })
              .catch((error) => {
                return res.status(400).json({
                  status: 'error',
                  message: error.message
                })
              })
          }
        })
        .catch((error) => {
          return res.status(500).json({
            status: 'error',
            message: error.message
          })
        })
    }
  }

  static delete = async (req, res) => {
    const id = req.params.id;
    const roleUser = req.user.role;

    if (roleUser === 'user') {
      return res.status(401).json({
        status: "error",
        message: "user unauthorized"
      })
    } else {
      await Book.findOne({
        where: {
          id: id
        }
      })
        .then(async (result) => {
          if (result === null) {
            return res.status(404).json({
              status: 'error',
              message: 'Data not found'
            })
          } else {
            await Book.destroy({
              where: {
                id: id
              }
            })
              .then(() => {
                return res.status(200).json({
                  message: 'success delete author',
                })
              })
              .catch((err) => {
                return res.status(400).json({
                  status: 'error',
                  message: err.message
                })
              })
          }
        })
        .catch((err) => {
          return res.status(500).json({
            status: 'error',
            message: err.message
          })
        })
    }
  }
}

module.exports = bookController;