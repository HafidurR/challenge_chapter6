const { User } = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mail = require('../middlewares/forgotPassword');
const path = require('path');
const fs = require('fs');
const sendEmail = require('../middlewares/sendEmail');
const { JWT_SECRET_KEY } = process.env;

class userController {
  static getAllUser = async (req, res) => {
    const option = {
      attributes: ['id', 'name', 'email', 'birthdate', 'no_telp', 'image']
    }
    await User.findAll(option)
      .then((result) => {
        return res.status(200).json({
          status: 'success',
          user: result
        })
      })
      .catch((error) => {
        return res.status(400).json({
          status: 'error',
          message: error.message
        })
      })
  }

  static register = async (req, res) => {
    const { name, email, password, birthdate, no_telp, address, role } = req.body;

    const hash = await bcrypt.hash(password, 10);
    await User.create({
      name, email, password: hash, birthdate, no_telp, address, role, image: "avatar.png"
    })
      .then((result) => {
        let rsl = {
          id: result.id,
          name: result.name,
          email: result.email,
          birthdate: result.birthdate,
          no_telp: result.no_telp,
          address: result.address,
          role: result.role,
          image: result.image
        }
        sendEmail(email, name);
        return res.status(201).json({
          status: 'success',
          message: 'success register',
          data: rsl
        })
      })
      .catch((error) => {
        return res.status(400).json({
          status: 'error',
          message: error.message
        })
      })
  }

  static login = async (req, res) => {
    const {
      email,
      password
    } = req.body;

    await User.findOne({
      where: {
        email
      }
    }).then(async result => {
      //return console.log(result);
      if (result === null) {
        return res.status(404).json({
          status: 'error',
          message: 'User not found'
        })
      } else {
        const data = {
          id: result.id,
          email: result.email,
          role: result.role
        }
        // return console.log(data);
        bcrypt.compare(password, result.password, async function (err, passed) {
          if (passed) {
            let token = jwt.sign(data, JWT_SECRET_KEY, {
              expiresIn: '1h'
            });
            return res.status(200).json({
              token
            })
          } else {
            return res.status(403).json({
              status: 'error',
              message: 'password is wrong'
            });
          }
        })
      }

    }).catch(error => {
      return res.status(400).json({
        status: 'error',
        message: error.message
      })
    })
  }

  static update_user = async (req, res) => {
    const id = req.params.id;
    const { name, email, password, birthdate, no_telp, address, role } = req.body;

    await User.findOne({
      where: {
        id: id
      }
    })
      .then(async (rsl) => {
        if (rsl === null) {
          return res.status(404).json({
            status: 'error',
            message: 'Data not found'
          })
        } else {
          await User.update({
            name, email, password, birthdate, no_telp, address, role
          }, {
            where: {
              id: id
            }
          })
            .then(async () => {
              await User.findOne({
                where: {
                  id: id
                }
              })
                .then(result => {
                  return res.status(200).json({
                    message: 'success update user',
                    User: result
                  })
                }).catch(err => {
                  return res.status(400).json({
                    message: err.message
                  })
                })
            })
            .catch((error) => {
              return res.status(400).json({
                status: 'error',
                message: error.message
              })
            })
        }
      })
      .catch((error) => {
        return res.status(500).json({
          status: 'error',
          message: error.message
        })
      })
  }

  static update_user_image = async (req, res) => {
    const id = req.params.id;

    let url = req.file.path.split('\\')
    url.shift()
    url = url.join('/')

    const data = await User.findOne({
      where: {
        id: id
      }
    })
    if (data === null) {
      return res.status(404).json({
        status: 'error',
        message: 'Data not found'
      });
    } else {
      await User.update({
        image: url
      }, {
        where: {
          id
        }
      })
        .then(dt => {
          if (data.image !== null) {
            fs.unlinkSync(path.join(__dirname, '../public/' + data.image))
          }
          return res.json({
            status: 'success',
            message: 'image successfully updated',
            data: {
              image: `${req.protocol}://${req.get('host')}/${url}`
            }
          });
        })
        .catch(error => {
          fs.unlinkSync(path.join(__dirname, '../public' + data.image));
          return res.status(400).json({
            status: 'error',
            message: error.message
          })
        })
    }
  }

  static delete = async (req, res) => {
    const id = req.params.id;

    await User.findOne({
      where: {
        id: id
      }
    })
      .then(async (result) => {
        if (result === null) {
          return res.status(404).json({
            status: 'error',
            message: 'Data not found'
          })
        } else {
          await User.destroy({
            where: {
              id: id
            }
          })
            .then(() => {
              return res.status(200).json({
                message: 'success delete user',
              })
            })
            .catch((err) => {
              return res.status(400).json({
                status: 'error',
                message: err.message
              })
            })
        }
      })
      .catch((err) => {
        return res.status(500).json({
          status: 'error',
          message: err.message
        })
      })

  }

  static async forgotPassword(req, res, next) {
    try {
      const { email } = req.body;

      const data = await User.findOne({
        where: { email }
      });

      if (!data) {
        throw {
          code: "ERR_BAD_REQUEST",
          message: "email is not registered"
        }
      } else {
        await mail(email, "Forgot password login", true, isSended => {
          if (isSended) {
            return res.json({
              success: true,
              message: "OTP code successfully sended to your email"
            })
          }
        })
      }
    } catch (error) {
      throw {
        code: "ERR_BAD_REQUEST",
        message: error.message
      }
    }
  }

  static async changePasswordByOTP(req, res, next) {
    try {
      const { otp, email } = req.params;
      const { newPassword } = req.body;

      const data = await User.findOne({
        where: { otp, email }
      });

      if (!data) {
        throw {
          code: "ERR_BAD_REQUEST",
          message: "email atau otp salah"
        }
      } else {
        const isPassed = bcrypt.compareSync(newPassword, data.password);
        if (isPassed) {
          throw {
            code: "ERR_BAD_REQUEST",
            message: "password baru harus berbeda dengan password sebelumnya"
          }
        }

        await User.update({
          password: bcrypt.hashSync(newPassword, 10),
          otp: null
        }, { where: { email } })
          .then(() => {
            return res.json({
              success: true,
              message: "password successfully chenged"
            })
          })
          .catch(error => {
            throw {
              code: "ERR_BAD_REQUEST",
              message: error.message
            }
          })
      }

    } catch (error) {
      next(error);
    }
  }

  static async verifyOTP(req, res, next) {
    try {
      const { otp } = req.params;

      const data = await User.findOne({
        where: { otp },
        attributes: ['email', 'otp']
      });

      if (!data) {
        throw {
          code: "ERR_BAD_REQUEST",
          message: "OTP code wrong"
        }
      } else {
        return res.json({
          success: true,
          message: "OTP code verified",
          data
        })
      }
    } catch (error) {
      next(error);
    }
  }



}
// 

module.exports = userController;