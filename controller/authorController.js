const { Author } = require('../models');

class authorController {
  static getAll = async (req, res) => {
    let { page, row } = req.query;

    page -= 1;
    await Author.findAll({
      attributes: ['id', 'name', 'address'],
      offset: page,
      limit: row
    })
      .then((result) => {
        return res.status(200).json({
          message: 'success get all data',
          author: result
        })
      })
      .catch((err) => {
        return res.status(500).json({
          status: 'error',
          message: err.message
        })
      })
  }

  static getByID = async (req, res) => {
    const id = req.params.id;
    const roleUser = req.user.role;

    if (roleUser === 'user') {
      return res.status(401).json({
        status: "error",
        message: "user unauthorized"
      })
    } else {
      await Author.findOne({
        where: {
          id: id
        }
      })
        .then((result) => {
          if (result === null) {
            return res.status(404).json({
              status: 'error',
              message: 'Data not found'
            })
          } else {
            return res.status(200).json({
              message: 'success get by ID',
              author: result
            })
          }
        })
        .catch((err) => {
          return res.status(500).json({
            status: 'error',
            message: err.message
          })
        })
    }
  }

  static create = async (req, res) => {
    const { name, birthdate, gender, address } = req.body;
    const roleUser = req.user.role;
    // return console.log(roleUser);
    if (roleUser === 'user') {
      return res.status(401).json({
        status: "error",
        message: "user unauthorized"
      })
    } else {
      await Author.create({
        name, birthdate, gender, address
      })
        .then((result) => {
          return res.status(201).json({
            status: 'success',
            message: 'success create new author',
            author: result
          })
        })
        .catch((error) => {
          const err = error.errors
          // console.log(err);
          const errorList = err.map(d => {
            let obj = {}
            // console.log(obj[d.path]);
            obj[d.path] = d.message
            return obj;
          })
          return res.status(400).json({
            status: 'error',
            message: errorList
          })
        })
    }
  }

  static update = async (req, res) => {
    const id = req.params.id;
    const { name, birthdate, gender, address } = req.body;
    const roleUser = req.user.role;

    if (roleUser === 'user') {
      return res.status(401).json({
        status: "error",
        message: "user unauthorized"
      })
    } else {
      await Author.findOne({
        where: {
          id: id
        }
      })
        .then(async (rsl) => {
          if (rsl === null) {
            return res.status(404).json({
              status: 'error',
              message: 'Data not found'
            })
          } else {
            await Author.update({
              name, birthdate, gender, address
            }, {
              where: {
                id: id
              }
            })
              .then(async () => {
                await Author.findOne({
                  where: {
                    id: id
                  }
                })
                  .then(result => {
                    return res.status(200).json({
                      message: 'success update author',
                      Author: result
                    })
                  }).catch(err => {
                    return res.status(400).json({
                      message: err.message
                    })
                  })
              })
              .catch((error) => {
                return res.status(400).json({
                  status: 'error',
                  message: error.message
                })
              })
          }
        })
        .catch((error) => {
          return res.status(500).json({
            status: 'error',
            message: error.message
          })
        })
    }
  }

  static delete = async (req, res) => {
    const id = req.params.id;
    const roleUser = req.user.role;

    if (roleUser === 'user') {
      return res.status(401).json({
        status: "error",
        message: "user unauthorized"
      })
    } else {
      await Author.findOne({
        where: {
          id: id
        }
      })
        .then(async (result) => {
          if (result === null) {
            return res.status(404).json({
              status: 'error',
              message: 'Data not found'
            })
          } else {
            await Author.destroy({
              where: {
                id: id
              }
            })
              .then(() => {
                return res.status(200).json({
                  message: 'success delete author',
                })
              })
              .catch((err) => {
                return res.status(400).json({
                  status: 'error',
                  message: err.message
                })
              })
          }
        })
        .catch((err) => {
          return res.status(500).json({
            status: 'error',
            message: err.message
          })
        })

    }
  }
}

module.exports = authorController;