const router        = require('express').Router();
const authorRouter  = require('../controller/authorController');
const verifyToken   = require('../middlewares/verifyToken');

router.get('/', authorRouter.getAll);
router.get('/:id', verifyToken, authorRouter.getByID);
router.post('/', verifyToken, authorRouter.create);
router.put('/:id', verifyToken, authorRouter.update);
router.delete('/:id', verifyToken, authorRouter.delete);

module.exports = router;