const router      = require('express').Router();
const userRoute   = require('../controller/userController');
const multer      = require('multer');
const { upload_cloudinary } = require('../middlewares/cloudinary');
const { update_user_image } = require('../controller/userController');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/images');
  },
  filename: (req, file, cb) => {
    const index = file.originalname.split('.').length;
    cb(null, Date.now() + '.' + file.originalname.split('.')[index - 1]);
  }
});

const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "image/png" ||
      file.mimetype === "image/jpg" ||
      file.mimetype === "image/jpeg"
    )
      return cb(null, true);
    cb(null, false);
    cb(new Error("Wrong filetype"));
  },
});

router.get('/', userRoute.getAllUser);
router.get('/verify-otp/:otp', userRoute.verifyOTP);

router.post('/register', userRoute.register);
router.post('/login', userRoute.login);
router.post('/forgot-password', userRoute.forgotPassword);

router.put('/:id', userRoute.update_user);
router.put('/image/:id', upload.single('image'), upload_cloudinary, update_user_image);
router.put('/:email/change-password/:otp', userRoute.changePasswordByOTP);
module.exports = router;