const router = require('express').Router();
const multer = require('multer');
const {upload_video_cloudinary} = require('../middlewares/cloudinary');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/video');
  },
  filename: (req, file, cb) => {
    const index = file.originalname.split('.').length;
    cb(null, Date.now() + '.' + file.originalname.split('.')[index - 1]);
  }
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 10000000,
  },
  fileFilter(req, file, cb) {
    if (!file.originalname.match(/\.(mp4|MPEG-4|mkv)$/)) {
      return cb(new Error("Please upload a video"));
    }
    cb(undefined, true);
  },
});

router.post('/', upload.single('video'), upload_video_cloudinary, (req, res) => {
  let url = req.file.path.split('\\')
  url.shift()
  url = url.join('/')

  let data = { video : url }

  res.send({
    message: 'success upload vidio',
    path: `${req.protocol}://${req.get('host')}/${data.video}`
  })
})

module.exports = router;